#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:134217728:799a16c92955b239ab8bddb2009b1eac30a6a4ee; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:33554432:873aae36ebb8201ebe2fd06b724af8db9dd9f55b \
          --target EMMC:/dev/block/by-name/recovery:134217728:799a16c92955b239ab8bddb2009b1eac30a6a4ee && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
